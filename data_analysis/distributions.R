#Author: Harish Sukhwani (hvs2@duke.edu)
#This file had density, distribution and quantile function 
#for the following distributions:
#Erlang (2,3 stage), HypoExp (2,3 stage)

"derlang_2" <- function(x, l = 1) {
  f <- dgamma(x, 2, l)
  f
}

"perlang_2" <- function(q, l = 1) {
  f <- pgamma(q, 2, l)
  f
}

"qerlang_2" <- function(p, l = 1) {
  f <- qgamma(p, 2, l)
  f
}

"derlang_3" <- function(x, l = 1) {
  f <- dgamma(x, 3, l)
  f
}

"perlang_3" <- function(q, l = 1) {
  f <- pgamma(q, 3, l)
  f
}

"qerlang_3" <- function(p, l = 1) {
  f <- qgamma(p, 3, l)
  f
}

"dhypoexp_2" <- function(x, l1, l2) {
  f <- dhypoexp(x, rate=c(l1,l2))
  f
}

"phypoexp_2" <- function(q, l1, l2) {
  f <- phypoexp(q, rate=c(l1,l2))
  f
}

"qhypoexp_2" <- function(p, l1, l2) {
  f <- qhypoexp(p, rate=c(l1,l2), interval = c(0.0, 1.0e+10))
  f
}

"dhypoexp_3" <- function(x, l1, l2, l3) {
  f <- dhypoexp(x, rate=c(l1,l2,l3))
  f
}

"phypoexp_3" <- function(q, l1, l2, l3) {
  f <- phypoexp(q, rate=c(l1,l2,l3))
  f
}

"qhypoexp_3" <- function(p, l1, l2,l3) {
  f <- qhypoexp(p, rate=c(l1,l2,l3), interval = c(0.0, 1.0e+10))
  f
}
