Harish Sukhwani (h (dot) sukhwani AT gmail (dot) com)  
Oct. 2, 2018

In this repository, we share the source code and datasets from our work on developing Stochastic models for Hyperledger Fabric.  

Hyperledger Fabric V1 is analyzed in [2] and v0.6 is analyzed in [3]. Thesis [1] covers both the papers along with additional empirical analysis.  

#### References
[1] Harish Sukhwani. *Performance Modeling & Analysis of Hyperledger Fabric (Permissioned Blockchain Network).* PhD thesis, Duke University, Dec. 2018. (in preparation).  
[2] H. Sukhwani, Nan Wang, Kishor S. Trivedi, and Andy Rindos. Performance Modeling of Hyperledger Fabric (Permissioned Blockchain Network). In *IEEE International Symposium on Network Computing and Applications (NCA)*, 2018.  
[3] H. Sukhwani, José M. Martı́nez, Xiaolin Chang, Kishor S. Trivedi, and Andy Rindos. Performance Modeling of PBFT Consensus Process for Permissioned Blockchain Network (Hyperledger Fabric). In *IEEE International Symposium on Reliable Distributed Systems (SRDS)*, Sept. 2017.
