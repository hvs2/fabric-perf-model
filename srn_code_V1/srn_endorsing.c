#include <stdio.h>
#include "user.h"

/* global variables */ 
double RATE_ENDOR = 0.308;

int guard_end();
int PendingP0();
int PendingP1();
int PendingWait();

void options() { 
    iopt(IOP_PR_RSET, VAL_YES);
    iopt(IOP_PR_RGRAPH, VAL_YES);
    iopt(IOP_PR_FULL_MARK, VAL_YES); 
}

int guard_end() {
    if (mark("P_wait") > 0) {
        return 1;
    } else {
        return 0;
    }
}

int PendingP0() {
    return (mark("P_en0"));
}

int PendingP1() {
    return (mark("P_en1"));
}

int PendingWait() {
    return (mark("P_wait"));
}

void net() { 
    /* Places and Transitions */
    place("Pend");
    place("P_en0"); init("P_en0",1);
    place("P_en1"); init("P_en1",1);
    place("P_wait");  

    imm("I_wait");
    guard("I_wait",guard_end);
    priority("I_wait",1);
    probval("I_wait",1);

    rateval("T_en1",RATE_ENDOR);
    rateval("T_en0",RATE_ENDOR);

    /* Arcs */
    iarc("T_en0","P_en0");
    iarc("T_en1","P_en1");
    viarc("I_wait","P_wait",PendingWait);
    viarc("I_wait","P_en0",PendingP0);
    viarc("I_wait","P_en1",PendingP1);
    oarc("T_en0","P_wait");
    oarc("T_en1","P_wait");
    oarc("I_wait","Pend");
} 

int assert() { 

} 

void ac_init() { 
    pr_net_info();
} 

void ac_reach() { 
    pr_rg_info();
} 


void ac_final() {
    int loop; 
    solve(INFINITY);
    pr_mtta("time to absorption");
} 


