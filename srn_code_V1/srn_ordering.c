#include <stdio.h>
#include "user.h"
#include <stdlib.h>
#include <time.h>

/* global variables */ 
#define BLOCK_SIZE 120
#define TX_ARRIVAL 500

#define QUEUE_SIZE BLOCK_SIZE*2 
#define TIMEOUT 2
#define ERLANG_STAGES 5
#define ERL_STAGE_RATE 2.5 // ERLANG_STAGES/TIMEOUT

void options() {
    iopt(IOP_SSMETHOD, VAL_POWER);
    iopt(IOP_SIMULATION,VAL_NO);
}

double holdingTime_Erlang () {
    if (mark("postfire") < ERLANG_STAGES) {
        return (1.0);
    } else 
        return (0.0);
}

double holdingTime1 () {
    if (mark("end_timeout") == 0) {
        return (1.0);
    } else 
        return (0.0);
}

double holdingTime2 () {
    if (mark("end_size") == 0) {
        return (1.0);
    } else 
        return (0.0);
}

int guard_batch_size() {
    if (mark("end_timeout") == 1 || mark("end_size") == 1) {
        return 0;
    } else {
        return 1;
    }
}

int guard_timeout() {
    if (mark("postfire") == ERLANG_STAGES && mark("end_timeout") == 0 && mark("end_size") == 0) {
        return 1;
    } else {
        return 0;
    }
}

int timeout_trigger_fn() {
    if (mark("ordering_service") > 0) {
        return 1;
    } else {
        return 0;
    }
}

void net() {
    /* Main NET */
    rateval("tx_arrival", TX_ARRIVAL);
    mharc("tx_arrival", "end_timeout", 1);
    mharc("tx_arrival", "end_size", 1);
            
    place("ordering_service");
    /* Timeout case */
    imm("check_timeout");
    guard("check_timeout", guard_timeout);
    priority("check_timeout", 1);
    place("end_timeout");
    oarc("tx_arrival", "ordering_service");
    iarc("check_timeout", "ordering_service");
    oarc("check_timeout", "end_timeout");
    /* Size case */
    imm("batch_size");
    guard("batch_size", guard_batch_size);
    priority("batch_size", 1);
    place("end_size");
    miarc("batch_size", "ordering_service", BLOCK_SIZE);
    oarc("batch_size", "end_size");
    
    /* Net for Deterministic timeout */
    place("TO_start");
    init("TO_start", 1);
    imm("timeout_trigger");
    priority("timeout_trigger", 1);
    guard("timeout_trigger", timeout_trigger_fn);

    place("prefire");
    place("postfire");
    rateval("timeout_stage", ERL_STAGE_RATE);
    iarc("timeout_trigger", "TO_start");
    moarc("timeout_trigger", "prefire", ERLANG_STAGES);
    iarc("timeout_stage", "prefire");
    oarc("timeout_stage", "postfire");
    mharc("timeout_trigger", "prefire", 1);
    mharc("timeout_trigger", "postfire", 1); 
}

int assert() {
}

void ac_init() {
    pr_net_info();
} 

void ac_reach() { 
    pr_rg_info();
}

double option_timeout() {
    return mark("end_timeout");
}

double option_size() {
    return mark("end_size");
}

void ac_final() {
    solve(INFINITY);
    pr_expected("Prob. of completing timeout", option_timeout);
    pr_expected("Prob. of completing Rateval", option_size);
}
