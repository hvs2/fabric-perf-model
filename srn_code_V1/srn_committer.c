#include <stdio.h>
#include "user.h"
#include <stdlib.h>
#include <time.h>

/* global variables */ 
#define CORES 4
#define RATE_BLOCK_ARR 1.0
#define RATE_VSCC 396.197
#define RATE_MVCC 390.777	// Size = 40
//#define RATE_MVCC 196.08 // Size = 80
//#define RATE_MVCC 138.89 // Size = 120

#define BLOCK_SIZE 40
#define QUEUE_SIZE BLOCK_SIZE*2
#define RATE_LWRITE 4.8123	// Size = 40
//#define RATE_LWRITE 4.801 // Size = 80
//#define RATE_LWRITE 5.308 // Size = 120

void options() {
    //iopt(IOP_SSMETHOD, VAL_POWER);
    iopt(IOP_SIMULATION,VAL_NO);
}

double rate_vscc() {
    if (mark("vscc_check") <= CORES) {
        return (RATE_VSCC*mark("vscc_check"));
    } else {
        return (RATE_VSCC*CORES);
    }
}

double tput_arr() {
    return (rate("block_arrival"));
}

double util_arr() {
    return (enabled("block_arrival"));
}

double tput_vscc() {
    return (rate("vscc"));
}

double util_vscc() {
    return (enabled("vscc"));
}

double qlen_vscc() {
    return mark("vscc_check");
}

double tput_mvcc() {
    return (rate("mvcc"));
}

double util_mvcc() {
    return (enabled("mvcc"));
}

double qlen_mvcc() {
    return mark("mvcc_check");
}

double tput_lwrite() {
    return (rate("lwrite"));
}

double util_lwrite() {
    return (enabled("lwrite"));
}

double qlen_lwrite() {
    return mark("ledger_write");
}

double incoming_queue_full() {
    if (mark("vscc_check") == QUEUE_SIZE) {
        return 1;
    } else {
        return 0;
    }
}

void net() {
    /* Places and Transitions */
    rateval("block_arrival", RATE_BLOCK_ARR);
    place("vscc_check");
    ratefun("vscc", rate_vscc);
    place("vscc_check_done");
    imm("vscc_collect");
    priority("vscc_collect", 1);

    place("mvcc_check");
    rateval("mvcc", RATE_MVCC);
    place("ledger_write"); 
    rateval("lwrite", RATE_LWRITE);

    /* Arcs */
    mharc("block_arrival", "vscc_check", QUEUE_SIZE);
    moarc("block_arrival", "vscc_check", BLOCK_SIZE);
    iarc("vscc", "vscc_check");
    oarc("vscc", "vscc_check_done");
    mharc("vscc", "vscc_check_done", QUEUE_SIZE);
	    
    miarc("vscc_collect", "vscc_check_done", BLOCK_SIZE);
    oarc("vscc_collect", "mvcc_check");
    mharc("vscc_collect", "mvcc_check", 2);
    
    iarc("mvcc", "mvcc_check");
    oarc("mvcc", "ledger_write");
    mharc("mvcc", "ledger_write", 2);
        
    iarc("lwrite", "ledger_write");
}

/* metrics */

int assert() {

}

void ac_init() { 
/* Information on the net structure */ 
	pr_net_info();
} 

void ac_reach() { 
/* Information on the reachability graph */ 
	pr_rg_info();
}

void ac_final() {
    solve(INFINITY);
    pr_expected("Throughput at Block Arri", tput_arr);
    pr_expected("Utilizati. at Block Arri", util_arr);
    pr_expected("Incoming Queue Full     ", incoming_queue_full);
    pr_expected("Throughput at VSCC check", tput_vscc);
    pr_expected("Utilizati. at VSCC check", util_vscc);
    pr_expected("Queue Len. at VSCC check", qlen_vscc);
    pr_expected("Throughput at RW   check", tput_mvcc);
    pr_expected("Utilizati. at RW   check", util_mvcc);
    pr_expected("Queue Len. at RW   check", qlen_mvcc);
    pr_expected("Throughput at Ledger    ", tput_lwrite);
    pr_expected("Utilizati. at Ledger    ", util_lwrite);
    pr_expected("Queue Len. at Ledger    ", qlen_lwrite);
}
