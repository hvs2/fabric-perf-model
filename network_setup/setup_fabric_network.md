Last Update: July 10, 2018

Goal: Setup a Hyperledger Fabric Blockchain Network across 4 physical nodes, running 2 peers (1 peer per org)

These instructions were written for v1.1. For recent Fabric releases, proceed with caution.

![network](https://bitbucket.org/hvs2/fabric-perf-model/raw/master/network_setup/figs/lab_setup_v6.png)

## Installation on Hyperledger fabric
On each physical node, run the following setps

1) Basic steps

    `sudo apt install libtool libltdl-dev #Incase you see ltdl.h header file missiing`
    
2) Install docker-ce

Follow instructions from https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1

```shell
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
# sudo apt-get install docker-ce
# sudo apt-cache policy docker-ce to remove stale versions of docker-ce
# sudo apt autoremove to cleanup apt cache
sudo apt-get install docker-ce=18.03.1~ce-0~ubuntu
sudo docker run hello-world
sudo usermod -aG docker $USER
```

At this point, Logout and login. 

3) Install docker-compose 

Ref: https://docs.docker.com/compose/install/#install-compose
```shell
#sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

4) Install Go 1.9.1
```shell
sudo curl -O https://storage.googleapis.com/golang/go1.9.1.linux-amd64.tar.gz
sudo tar -xvf go1.9.1.linux-amd64.tar.gz
sudo mv go /usr/local
mkdir ~/go-workspace
echo 'export GOROOT=/usr/local/go' >> ~/.bashrc
echo 'export GOPATH=$HOME/go-workspace' >> ~/.bashrc
echo 'export PATH=$PATH:$GOROOT/bin:$GOPATH/bin' >> ~/.bashrc
source ~/.bashrc
```

5) Install Python 2.7
```shell
sudo apt-get install python2.7
sudo apt-get install python-pip
pip install pyyaml
```

6) Install Fabric
```shell
mkdir -p $GOPATH/src/github.com/hyperledger
cd $GOPATH/src/github.com/hyperledger
git clone http://gerrit.hyperledger.org/r/fabric
cd fabric
git checkout v1.1.0
make release
make gotools #Adding this for now
make docker
make peer
make orderer
```
While doing `make docker`, if you see the error `Cannot connect to the Docker daemon`, you probably forgot to do `sudo usermod -aG docker $USER`. After this, logout and login.


```shell
sudo mkdir -p /var/hyperledger/product
sudo chown -R $(whoami): /var/hyperledger
```
Add the following to .bashrc and `source ~/.bashrc`
```shell
export PATH=$PATH:$GOPATH/src/github.com/hyperledger/fabric/build/bin/
```

## Create Docker Swarm network between peers
On one of the machines
```shell
docker swarm init --advertise-addr "ip_addr"
docker network create --attachable --driver overlay my-net
```

On the rest of machines
```shell
docker swarm join --token "token_id" ip_addr:2377
```

Once all done
```shell
docker swarm leave
```

## Making changes to Fabric source code
On your laptop, do the following

1) Patch the changes (from diff\_file) to `$GOPATH/src/github.com/hyperledger/fabric\`
2) Create new Docker images
```shell
cd build/image/peer/
docker build -t hyperledger/fabric-peer:x86_64-1.1.x .

cd build/image/orderer/
docker build -t hyperledger/fabric-orderer:x86_64-1.1.x .
```
Note that the above is the new docker image name

3) Export the docker images
```shell
docker save -o <path for generated tar file> <image name>
```
scp the .tar file to all nodes. There, do the following
```shell
docker load -i <path to image tar file>
```

## Miscellaneous useful commands
Docker stop and remove
```shell
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
```

```shell
sudo service docker stop
sudo rm -rf /var/lib/docker/network
sudo systemctl start docker
```

To start docker daemon (for caliper to fetch docker stats)
(Ref: https://docs.docker.com/install/linux/linux-postinstall/)
`sudo systemctl edit docker.service` add content
```shell
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2375
```
`sudo systemctl daemon-reload` and `sudo systemctl restart docker.service`. When you see dockerd (`sudo ps aux |grep dockerd`), you will see `/usr/bin/dockerd -H fd:// -H tcp://0.0.0.0:2375`. 

ERROR: golint unrecognized import path
```shell
mkdir -p $GOPATH/src/golang.org/x   && git clone https://github.com/golang/lint.git $GOPATH/src/golang.org/x/lint   && go get -u golang.org/x/lint/golint
make gotools
```
