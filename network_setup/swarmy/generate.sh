#!/usr/bin/env bash
#~/go-workspace/src/github.com/hyperledger/fabric/build/bin/cryptogen generate --config=./crypto-config.yaml
~/go-workspace/src/github.com/hyperledger/fabric/build/bin/configtxgen -profile TwoOrgsOrdererGenesis -outputBlock twoorgs.genesis.block
~/go-workspace/src/github.com/hyperledger/fabric/build/bin/configtxgen -profile TwoOrgsChannel -outputCreateChannelTx mychannel.tx -channelID mychannel
