docker run --detach -it --network="my-net" \
--name ca0 \
-p 7054:7054 \
-e FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server \
-e FABRIC_CA_SERVER_CA_NAME=ca-org1 \
-e FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.org1.example.com-cert.pem \
-e FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/e1821201f3c1b17456cbb96cfc98c78edde081e78ba97f382529a7749eb2dbf9_sk \
-e CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=my-net \
-v $(pwd)/crypto-config/peerOrganizations/org1.example.com/ca/:/etc/hyperledger/fabric-ca-server-config \
hyperledger/fabric-ca:x86_64-1.1.0 \
sh -c 'fabric-ca-server start -b admin:adminpw -d'
