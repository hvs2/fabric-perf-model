docker run --detach -it \
--network="my-net" \
--name peer0.org1.example.com \
--link orderer.example.com:orderer.example.com \
-p 7051:7051 \
-p 7053:7053 \
-e CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock \
-e CORE_PEER_ID=peer0.org1.example.com \
-e CORE_PEER_ENDORSER_ENABLED=true \
-e CORE_PEER_LOCALMSPID=Org1MSP \
-e CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/peer/msp/ \
-e CORE_PEER_ADDRESS=peer0.org1.example.com:7051 \
-e CORE_PEER_GOSSIP_USELEADERELECTION=true \
-e CORE_PEER_GOSSIP_ORGLEADER=false \
-e CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer0.org1.example.com:7051 \
-e CORE_PEER_TLS_ENABLED=true \
-e CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/msp/peer/tls/server.key \
-e CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/msp/peer/tls/server.crt \
-e CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/msp/peer/tls/ca.crt \
-e CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=my-net \
-v /var/run/:/host/var/run/ \
-v $(pwd)/crypto-config/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/:/etc/hyperledger/msp/peer \
-w /opt/gopath/src/github.com/hyperledger/fabric \
hyperledger/fabric-peer:x86_64-1.1.4x \
peer node start
