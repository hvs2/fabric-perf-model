docker run --detach -it --network="my-net" \
--name ca1 \
-p 8054:7054 \
-e FABRIC_CA_HOME=/etc/hyperledger/fabric-ca-server \
-e FABRIC_CA_SERVER_CA_NAME=ca-org2 \
-e FABRIC_CA_SERVER_CA_CERTFILE=/etc/hyperledger/fabric-ca-server-config/ca.org2.example.com-cert.pem \
-e FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/83ba1f077850ca5a7a534124dadd874beb677b379cda496bcac62c8b37424d97_sk \
-e CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=my-net \
-v $(pwd)/crypto-config/peerOrganizations/org2.example.com/ca/:/etc/hyperledger/fabric-ca-server-config \
hyperledger/fabric-ca:x86_64-1.1.0 \
sh -c 'fabric-ca-server start -b admin:adminpw -d'
