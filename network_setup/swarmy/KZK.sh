docker run --detach -it --network="my-net" \
--name zookeeper0 \
-p '2181' \
-p '2888' \
-p '3888' \
-e ZOO_MY_ID=1 \
-e ZOO_SERVERS='server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888' \
hyperledger/fabric-zookeeper:latest

docker run --detach -it --network="my-net" \
--name zookeeper1 \
-p '2181' \
-p '2888' \
-p '3888' \
-e ZOO_MY_ID=2 \
-e ZOO_SERVERS='server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888' \
hyperledger/fabric-zookeeper:latest

docker run --detach -it --network="my-net" \
--name zookeeper2 \
-p '2181' \
-p '2888' \
-p '3888' \
-e ZOO_MY_ID=3 \
-e ZOO_SERVERS='server.1=zookeeper0:2888:3888 server.2=zookeeper1:2888:3888 server.3=zookeeper2:2888:3888' \
hyperledger/fabric-zookeeper:latest


docker run --detach -it --network="my-net" \
--name kafka0 \
-p '9092' \
-e KAFKA_MESSAGE_MAX_BYTES=103809024 \
-e KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 \
-e KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false \
-e KAFKA_BROKER_ID=0 \
-e KAFKA_MIN_INSYNC_REPLICAS=2 \
-e KAFKA_DEFAULT_REPLICATION_FACTOR=3 \
-e KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181 \
--link zookeeper0:zookeeper0 \
--link zookeeper1:zookeeper1 \
--link zookeeper2:zookeeper2 \
hyperledger/fabric-kafka:latest

docker run --detach -it --network="my-net" \
--name kafka1 \
-p '9092' \
-e KAFKA_MESSAGE_MAX_BYTES=103809024 \
-e KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 \
-e KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false \
-e KAFKA_BROKER_ID=1 \
-e KAFKA_MIN_INSYNC_REPLICAS=2 \
-e KAFKA_DEFAULT_REPLICATION_FACTOR=3 \
-e KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181 \
--link zookeeper0:zookeeper0 \
--link zookeeper1:zookeeper1 \
--link zookeeper2:zookeeper2 \
hyperledger/fabric-kafka:latest

docker run --detach -it --network="my-net" \
--name kafka2 \
-p '9092' \
-e KAFKA_MESSAGE_MAX_BYTES=103809024 \
-e KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 \
-e KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false \
-e KAFKA_BROKER_ID=2 \
-e KAFKA_MIN_INSYNC_REPLICAS=2 \
-e KAFKA_DEFAULT_REPLICATION_FACTOR=3 \
-e KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181 \
--link zookeeper0:zookeeper0 \
--link zookeeper1:zookeeper1 \
--link zookeeper2:zookeeper2 \
hyperledger/fabric-kafka:latest

docker run --detach -it --network="my-net" \
--name kafka3 \
-p '9092' \
-e KAFKA_MESSAGE_MAX_BYTES=103809024 \
-e KAFKA_REPLICA_FETCH_MAX_BYTES=103809024 \
-e KAFKA_UNCLEAN_LEADER_ELECTION_ENABLE=false \
-e KAFKA_BROKER_ID=3 \
-e KAFKA_MIN_INSYNC_REPLICAS=2 \
-e KAFKA_DEFAULT_REPLICATION_FACTOR=3 \
-e KAFKA_ZOOKEEPER_CONNECT=zookeeper0:2181,zookeeper1:2181,zookeeper2:2181 \
--link zookeeper0:zookeeper0 \
--link zookeeper1:zookeeper1 \
--link zookeeper2:zookeeper2 \
hyperledger/fabric-kafka:latest
