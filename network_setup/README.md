Last Update: Dec 19, 2018

Goal: Setup a Hyperledger Fabric network across 4 physical nodes, running 2 peers (1 peer per org). Setup Hyperledger caliper on one peer for performance benchmarking. 

These instructions were written for Fabric v1.1, released in March '18. I used this extensively from June-July '18. I will update this repo. after enhancing these scripts for recent Fabric releases. 

1. Install Fabric on all but one node - Follow instructions from setup_fabric_network.md, install Fabric each nodes. Then create a Docker Swarm network. 

2. Install Caliper on the remaining node - Follow instructions from their website

3. Edit scripts in swarmy/ and (optional) regenerate crypto files
	- configtx.yaml
	- crypto-config.yaml
	- run ./generate.sh (if you need to regenerate crypto files, remove comment on the first line). This generates mychannel.tx and twoorgs.genesis.block
	- review docker settings in each *.sh file 
	
	Note that the docker-compose.yaml script is only for reference. The same settings are used in various *.sh files.
	
4. In setup_fabric.py and setup_caliper, edit hostnames in remote

5. Ensure that the private keys for the respective servers are installed locally

6. Edit Caliper config files
	a. Add hostnames and host-ip addresses
	b. Change crypto-key and cert paths (if crypto files regenerated)
	c. Change txDuration and rate depending on the SUT
	
7. Copy the caliper config files in ~/<caliper>/benchmark/simple/

8. Setup and start Fabric network + Caliper
```shell
./setup_fabric.py setup
./setup_caliper.py setup
./setup_fabric.py start
```

9. On the node running Caliper, goto ~/<caliper>/benchmark/simple, and run the following to start Caliper 
```shell
node main.js -c config-swarmy.json -n fabric-swarmy.json
```
.. wait for the tests to finish

10. Stop Fabric and transfer datasets from node running Caliper
```shell
# Following order is important
./setup_fabric.py stop
./setup_caliper.py transfer remote_folder local_folder
```


