#! /usr/bin/env python
import sys
import os
import paramiko
import yaml
import subprocess
import time
from time import gmtime, strftime
from datetime import datetime
from pytz import timezone

pst = timezone('America/Los_Angeles')
pst_time = datetime.now(pst)
current_time = pst_time.strftime("%Y%m%dT%H%M%S")

user = '(username)'
default_remote_path = '~/'
remote = {
        "orderer"   : '(hostname)',
        "peer0"     : '(hostname)',
        "peer1"     : '(hostname)',
        }
ssh_clients = {}

#Setup SSH clients for all machines
for machine in remote.keys():
    try:
        ssh_clients[machine] = paramiko.SSHClient()
        ssh_clients[machine].set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_clients[machine].connect(hostname = remote[machine], username = user)
    except paramiko.SSHException:
        print "Connection to %s failed" % remote[machine]
    else:
        print "Connected to %s succeed" % remote[machine]

def _close_ssh_clients():
    for machine in remote.keys():
        ssh_clients[machine].close()

'''
setup
    scp swarmy.tar to all peers
    (future) scp docker\_images/peer-1.1.x.tar.gz to all peers // rarely
    docker stop
    docker rm
    remove old swarmy and untar swarmy.tar
'''
def setup():
    cwd = os.getcwd()
    print "Setting up Fabric"
    print "1) Generating tar for swarmy"
    tar_command = "tar cf swarmy.tar swarmy/"
    subprocess.call(tar_command, shell=True, cwd=cwd)
    print "2) scp to all machines"
    for machine in remote.keys():
        scp_command = "scp swarmy.tar %s@%s:%s" % (user, remote[machine], default_remote_path)
        print scp_command
        subprocess.call(scp_command, shell=True, cwd=cwd)
    print "3) Clean up docker instances"
    for machine in ['orderer', 'peer0', 'peer1']:
        try:
            print "stop docker instances at %s " % (machine)
            ssh_clients[machine].exec_command("docker stop $(docker ps -a -q)")
            time.sleep(10);
            print "reemove docker instances at %s " % (machine)
            ssh_clients[machine].exec_command("docker rm $(docker ps -a -q)")
        except paramiko.SSHException:
            print "Something went wrong with machine %s" % machine

    print "4) untar swarmy.tar"
    for machine in ['orderer', 'peer0', 'peer1']:
        try:
            print "at machine %s" % machine
            ssh_clients[machine].exec_command('cd ' + default_remote_path + '; rm -rf swarmy/;' + 'tar -xf swarmy.tar');
        except paramiko.SSHException:
            print "Something went wrong with machine %s" % machine

'''
start
	(note the order)
    orderer: launch KZK -> Orderer
    Peer0: launch ca0, peer0
    Peer1: launch ca1, peer1
'''
def start():
    print "starting KZK at orderer"
    stdin,stdout,stderr = ssh_clients['orderer'].exec_command("cd ~/swarmy; ./KZK.sh");
    print stderr.readlines()
    time.sleep(20);
    print "starting orderer at orderer"
    stdin,stdout,stderr = ssh_clients['orderer'].exec_command("cd ~/swarmy; ./orderer.sh");
    print stderr.readlines()
    time.sleep(10);
    print "starting CA0 and peer0 at peer0"
    stdin,stdout,stderr = ssh_clients['peer0'].exec_command("cd ~/swarmy; ./ca0.sh");
    print stderr.readlines()
    time.sleep(5);
    stdin,stdout,stderr = ssh_clients['peer0'].exec_command("cd ~/swarmy; ./peer0.sh");
    print stderr.readlines()
    time.sleep(10);
    print "starting CA1 and peer1 at peer1"
    stdin,stdout,stderr = ssh_clients['peer1'].exec_command("cd ~/swarmy; ./ca1.sh");
    print stderr.readlines()
    time.sleep(5);
    stdin,stdout,stderr = ssh_clients['peer1'].exec_command("cd ~/swarmy; ./peer1.sh");
    print stderr.readlines()
    time.sleep(10);

'''
stop_transfer
    capture docker log files
    stop container
    transfer log files to basefolder
'''
def _create_log_folder(foldername):
    folder_path = os.getcwd() + '/' + foldername
    if os.path.exists(folder_path):
        print "Directory %s already exists" % folder_path
        return
    try:
        os.makedirs(folder_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    print "Directory created = %s" % folder_path
    
def _generate_log(client, instance_name, timestamp):
    stdin,stdout,stderr = ssh_clients[client].exec_command('cd ~/swarmy; docker logs --timestamps ' + instance_name + ' > ' + instance_name + '-' + timestamp + '.log');
    print stderr.readlines()
    stdin,stdout,stderr = ssh_clients[client].exec_command('cd ~/swarmy; tar -zcf ' + instance_name + '.tgz ' +  '*.log');
    print stderr.readlines()
    stdin,stdout,stderr = ssh_clients[client].exec_command('cd ~/swarmy; rm -rf *.log');
    print stderr.readlines()

def _stop_docker(client):
    stdin,stdout,stderr = ssh_clients[client].exec_command('pwd; docker ps;')
    print stdout.readlines()
    print stderr.readlines()
    stdin,stdout,stderr = ssh_clients[client].exec_command("docker stop $(docker ps -a -q)");
    print stdout.readlines()
    print stderr.readlines()
    time.sleep(5);
    stdin,stdout,stderr = ssh_clients[client].exec_command('docker rm $(docker ps -a -q)');
    print stdout.readlines()
    print stderr.readlines()
    time.sleep(2);

def _transfer_log(client, folder_path):
    cwd = os.getcwd()
    scp_command = "scp %s@%s:%s %s" % (user, remote[client], default_remote_path + 'swarmy' + '/*.tgz', folder_path)
    print "scp_command = %s" % scp_command
    try:
        subprocess.call(scp_command, shell=True, cwd=cwd)
    except paramiko.SSHException:
        print "Something went wrong with machine %s" % machine

def stop():
    _create_log_folder('results/' + current_time)
    _generate_log('orderer', 'orderer.example.com', current_time)
    _generate_log('peer0', 'peer0.org1.example.com', current_time)
    _generate_log('peer1', 'peer0.org2.example.com', current_time)
    _stop_docker('peer0')
    _stop_docker('peer1')
    _stop_docker('orderer')
    _transfer_log('orderer', 'results/' + current_time)
    _transfer_log('peer0', 'results/' + current_time)
    _transfer_log('peer1', 'results/' + current_time)
    print "TODO: remove remote logs"

##############################################################################
if len(sys.argv) == 1:
    print "ERROR: No parameters defined. Exiting"
    sys.exit()

if sys.argv[1] == 'setup':
    setup()
    _close_ssh_clients()

if sys.argv[1] == 'start':
    start()
    _close_ssh_clients()

if sys.argv[1] == 'stop':
    stop()
    _close_ssh_clients()

if sys.argv[1] == 'test':
    print "nothing to test"
