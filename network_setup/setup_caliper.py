#! /usr/bin/env python
import sys
import os
import paramiko
import yaml
import subprocess
import time
from time import gmtime, strftime
from datetime import datetime
from pytz import timezone

user = '(username)'
default_remote_path = '~/go-workspace/src/github.com/hyperledger/caliper/'
default_network_path = default_remote_path + 'network/fabric/'
#default_private_key = paramiko.RSAKey.from_private_key_file("/home/harish/.ssh/id_rsa", password = "harish22")
remote = {
        "caliper"   : '(hostname)'
        }

ssh_clients = {}

#Setup SSH clients for all machines
for machine in remote.keys():
    try:
        ssh_clients[machine] = paramiko.SSHClient()
        ssh_clients[machine].set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_clients[machine].connect(hostname = remote[machine], username = user)
    except paramiko.SSHException:
        print "Connection to %s failed" % remote[machine]
    else:
        print "Connected to %s succeed" % remote[machine]

def _close_ssh_clients():
    for machine in remote.keys():
        ssh_clients[machine].close()

'''
setup
    scp swarmy.tar in <caliper>/network/fabric/
    untar swarmy.tar
    #only need crypto-config folder, so it can talk to Fabric peers/orderer
'''
def setup():
    cwd = os.getcwd()
    print "Setting up Caliper's network folder"
    print "1) scp to caliper"
    for machine in remote.keys():
        scp_command = "scp swarmy.tar %s@%s:%s" % (user, remote[machine], default_network_path)
        print scp_command
        subprocess.call(scp_command, shell=True, cwd=cwd)
    print "2) untar swarmy.tar"
    for machine in remote.keys():
        try:
            print "at machine %s" % machine
            ssh_clients[machine].exec_command('cd ' + default_network_path + '; rm -rf swarmy/;' + 'tar -xf swarmy.tar');
        except paramiko.SSHException:
            print "Something went wrong with machine %s" % machine

'''
start (future)
    goto benchmark/simple/
    start caliper
'''
def start():
    print "Future work"

'''
transfer remote_folder_name local_folder_name
    goto caliper's default_remote_path
    move getDefault.json file into remote_folder_name
    tar and send to local_folder_name
    untar in local folder using tag --strip-components 1
'''
def transfer(remote_folder, local_folder):
    stdin,stdout,stderr = ssh_clients['caliper'].exec_command( \
            "cd " + default_remote_path + ";" \
            + "mv getDefaultTx*.json "  + "../perf_results/" + remote_folder + ";" \
            + "mv output.txt " + "../perf_results/" + remote_folder + ";" \
            + "cd ../perf_results" + ";" \
            + "tar cf results.tar " + remote_folder + ";" \
            );
    print stderr.readlines()
    scp_command = "scp %s@%s:%s%s results/%s/" % (user, remote[machine], default_remote_path, "../perf_results/results.tar", local_folder)
    print scp_command
    subprocess.call(scp_command, shell=True, cwd=os.getcwd())
    untar_command = "cd results/%s; tar --strip-components 1 -xvf results.tar" % (local_folder)
    subprocess.call(untar_command, shell=True, cwd=os.getcwd())

##############################################################################
'''
Command: setup_caliper.py transfer caliper_folder_name local_folder_name
'''
if len(sys.argv) == 1:
    print "ERROR: No parameters defined. Exiting"
    sys.exit()

if sys.argv[1] == 'setup':
    setup()
    _close_ssh_clients()

if sys.argv[1] == 'start':
    start()
    _close_ssh_clients()

if sys.argv[1] == 'transfer':
    remote_folder = sys.argv[2]
    local_folder = sys.argv[3]
    transfer(remote_folder, local_folder)
    _close_ssh_clients()

if sys.argv[1] == 'test':
    print "nothing to test"
