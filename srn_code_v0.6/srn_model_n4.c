#include <stdio.h>
#include "user.h"
#include <stdlib.h>
#include <time.h>
#define PR12Val1 7.7448 
#define PR12Val2 1.50895 
#define PR3Val1 34.7047 
#define PR3Val2 1.202207 
#define TxVal1 1.41605 
#define TxVal2 2.09217 
#define PR3HypoVal1 2 
#define PR3HypoVal2 267.9746 
#define PR3HypoVal3 22.04979 
#define PR3HypoVal4 0 
#define QVal1 25.864 
#define QVal2 1.56081 
#define QueueRate 8.959 
int f = 1; 
int n = 4; 
double timeVal; 
int gPS0 () {
	if ((mark("PP_0p")>= 2*f)) return (1);
		else
	return (0);}
int gPS1 () {  
	if ((mark("PP_1p")>= (2*f-1)))	return (1);
    else
    return (0);
    }
int gPS2 () {  
	if ((mark("PP_2p")>= (2*f-1)))	return (1);
    else
    return (0);
    }
int gPS3 () {  
	if ((mark("PP_3p")>= (2*f-1)))	return (1);
    else
    return (0);
    }
int gM0 () {  
	if ((mark("P_0p")>= 2*f))	return (1);
    else
    return (0);
    }
int gM1 () {  
	if ((mark("P_1p")>= 2*f))	return (1);
    else
    return (0);
    }
int gM2 () {  
	if ((mark("P_2p")>= 2*f))	return (1);
    else
    return (0);
    }
int gM3 () {  
	if ((mark("P_3p")>= 2*f))	return (1);
    else
    return (0);
    }
int gCReply () { 
	if ((mark("PM0")+mark("PM1")+mark("PM2")+mark("PM3")>= (3*f+1)))
	return (1);
    else
    return (0);
    }
int myhalt () {
	if (mark("Cend") !=0)
		return (0);
	else
		return (1);
}
void options() { 
	iopt(IOP_SIMULATION,VAL_YES); 
	iopt(IOP_SIM_STD_REPORT,VAL_YES); 
	iopt(IOP_SIM_SEED,35983453) ; 
	iopt(IOP_SIM_RUNS, 5000); 
	fopt(FOP_SIM_ERROR, 0.1 ) ; 
	fopt(FOP_SIM_LENGTH, 20); 
	iopt(IOP_SIM_CUMULATIVE,VAL_NO); 
	fopt(FOP_SIM_CONFIDENCE,.9); 
}
void net() {
	place("PP_0");
	place("PP_0p");
	place("P_0");
	place("P_0p");
	place("M0");
	init("M0",1);
	place("PM0");
	place("PS0");
	init("PS0",1);
	place("PP_1");
	place("PP_1p");
	place("P_1");
	place("P_1p");
	place("M1");
	init("M1",1);
	place("PM1");
	place("PS1");
	init("PS1",1);
	place("PP_2");
	place("PP_2p");
	place("P_2");
	place("P_2p");
	place("M2");
	init("M2",1);
	place("PM2");
	place("PS2");
	init("PS2",1);
	place("PP_3");
	place("PP_3p");
	place("P_3");
	place("P_3p");
	place("M3");
	init("M3",1);
	place("PM3");
	place("PS3");
	init("PS3",1);
	place("PPS1_0");
	place("PPS1_2");
	place("PPS1_3");
	place("PPS2_0");
	place("PPS2_1");
	place("PPS2_3");
	place("PPS3_0");
	place("PPS3_1");
	place("PPS3_2");

	place("PS0_1");
	place("PS0_2");
	place("PS0_3");
	place("PS1_0");
	place("PS1_2");
	place("PS1_3");
	place("PS2_0");
	place("PS2_1");
	place("PS2_3");
	place("PS3_0");
	place("PS3_1");
	place("PS3_2");

	place("Cend");
	place("PPS1");
	place("N01");
	place("PPS2");
	place("N02");
	place("PPS3");
	place("N03");
	place("Pcl");
 	init("Pcl",1);
 	place("CLeader");
 	init("CLeader",1);
 	imm("tcend"); guard("tcend",gCReply);	priority("tcend",1); probval("tcend",1);
	weibval("TC0",PR12Val1, PR12Val2);
	weibval("TN1",TxVal1, TxVal2);
	weibval("TThink1_1",PR12Val1, PR12Val2);
	weibval("TN2",TxVal1, TxVal2);
	weibval("TThink1_2",PR12Val1, PR12Val2);
	weibval("TN3",TxVal1, TxVal2);
	weibval("TThink1_3",PR12Val1, PR12Val2);

	weibval("TPPS1_0",TxVal1, TxVal2);
	weibval("TPPS1_2",TxVal1, TxVal2);
	weibval("TPPS1_3",TxVal1, TxVal2);
	weibval("TPPS2_0",TxVal1, TxVal2);
	weibval("TPPS2_1",TxVal1, TxVal2);
	weibval("TPPS2_3",TxVal1, TxVal2);
	weibval("TPPS3_0",TxVal1, TxVal2);
	weibval("TPPS3_1",TxVal1, TxVal2);
	weibval("TPPS3_2",TxVal1, TxVal2);

	hypoval("TThink2_0",PR3HypoVal1, PR3HypoVal2,PR3HypoVal3, PR3HypoVal4);
	guard("TThink2_0",gPS0);
	hypoval("TThink2_1",PR3HypoVal1, PR3HypoVal2,PR3HypoVal3, PR3HypoVal4);
	guard("TThink2_1",gPS1);
	hypoval("TThink2_2",PR3HypoVal1, PR3HypoVal2,PR3HypoVal3, PR3HypoVal4);
	guard("TThink2_2",gPS2);
	hypoval("TThink2_3",PR3HypoVal1, PR3HypoVal2,PR3HypoVal3, PR3HypoVal4);
	guard("TThink2_3",gPS3);

	weibval("TPS0_1",TxVal1,TxVal2);
	weibval("TPS0_2",TxVal1,TxVal2);
	weibval("TPS0_3",TxVal1,TxVal2);
	weibval("TPS1_0",TxVal1,TxVal2);
	weibval("TPS1_2",TxVal1,TxVal2);
	weibval("TPS1_3",TxVal1,TxVal2);
	weibval("TPS2_0",TxVal1,TxVal2);
	weibval("TPS2_1",TxVal1,TxVal2);
	weibval("TPS2_3",TxVal1,TxVal2);
	weibval("TPS3_0",TxVal1,TxVal2);
	weibval("TPS3_1",TxVal1,TxVal2);
	weibval("TPS3_2",TxVal1,TxVal2);

	weibval("TP0",QVal1,QVal2);
	weibval("T0",QVal1,QVal2);
	imm("TM0");
	guard("TM0",gM0);
	priority("TM0",1);
	probval("TM0",1);
	weibval("TP1",QVal1,QVal2);
	weibval("T1",QVal1,QVal2);
	imm("TM1");
	guard("TM1",gM1);
	priority("TM1",1);
	probval("TM1",1);
	weibval("TP2",QVal1,QVal2);
	weibval("T2",QVal1,QVal2);
	imm("TM2");
	guard("TM2",gM2);
	priority("TM2",1);
	probval("TM2",1);
	weibval("TP3",QVal1,QVal2);
	weibval("T3",QVal1,QVal2);
	imm("TM3");
	guard("TM3",gM3);
	priority("TM3",1);
	probval("TM3",1);
	halting_condition(myhalt);
  /*  ======  ARC ====== */
	iarc("TM0","M0");
	oarc("TM0","PM0");
	iarc("TM1","M1");
	oarc("TM1","PM1");
	iarc("TM2","M2");
	oarc("TM2","PM2");
	iarc("TM3","M3");
	oarc("TM3","PM3");
	oarc("TN1","PPS1");
	oarc("TC0","N01");
	iarc("TN1","N01");
	oarc("TN2","PPS2");
	oarc("TC0","N02");
	iarc("TN2","N02");
	oarc("TN3","PPS3");
	oarc("TC0","N03");
	iarc("TN3","N03");
	iarc("TC0","CLeader");
	iarc("TPPS1_0","PPS1_0");
	iarc("TPPS1_2","PPS1_2");
	iarc("TPPS1_3","PPS1_3");
	iarc("TPPS2_0","PPS2_0");
	iarc("TPPS2_1","PPS2_1");
	iarc("TPPS2_3","PPS2_3");
	iarc("TPPS3_0","PPS3_0");
	iarc("TPPS3_1","PPS3_1");
	iarc("TPPS3_2","PPS3_2");

	iarc("TPS0_1","PS0_1");
	iarc("TPS0_2","PS0_2");
	iarc("TPS0_3","PS0_3");
	iarc("TPS1_0","PS1_0");
	iarc("TPS1_2","PS1_2");
	iarc("TPS1_3","PS1_3");
	iarc("TPS2_0","PS2_0");
	iarc("TPS2_1","PS2_1");
	iarc("TPS2_3","PS2_3");
	iarc("TPS3_0","PS3_0");
	iarc("TPS3_1","PS3_1");
	iarc("TPS3_2","PS3_2");
	iarc("tcend","Pcl");
	iarc("TThink1_1","PPS1");
	iarc("TThink1_2","PPS2");
	iarc("TThink1_3","PPS3");

	iarc("TThink2_0","PS0");
	iarc("TThink2_1","PS1");
	iarc("TThink2_2","PS2");
	iarc("TThink2_3","PS3");

	oarc("TPPS1_0","PP_0");
	oarc("TThink1_1","PPS1_0");
	oarc("TPPS1_2","PP_2");
	oarc("TThink1_1","PPS1_2");
	oarc("TPPS1_3","PP_3");
	oarc("TThink1_1","PPS1_3");
	oarc("TPPS2_0","PP_0");
	oarc("TThink1_2","PPS2_0");
	oarc("TPPS2_1","PP_1");
	oarc("TThink1_2","PPS2_1");
	oarc("TPPS2_3","PP_3");
	oarc("TThink1_2","PPS2_3");
	oarc("TPPS3_0","PP_0");
	oarc("TThink1_3","PPS3_0");
	oarc("TPPS3_1","PP_1");
	oarc("TThink1_3","PPS3_1");
	oarc("TPPS3_2","PP_2");
	oarc("TThink1_3","PPS3_2");

	iarc("TP0","PP_0");
	oarc("TP0","PP_0p");
	iarc("TP1","PP_1");
	oarc("TP1","PP_1p");
	iarc("TP2","PP_2");
	oarc("TP2","PP_2p");
	iarc("TP3","PP_3");
	oarc("TP3","PP_3p");

	oarc("TThink2_0","PS0_1");
	oarc("TPS0_1","P_1");
	oarc("TThink2_0","PS0_2");
	oarc("TPS0_2","P_2");
	oarc("TThink2_0","PS0_3");
	oarc("TPS0_3","P_3");
	oarc("TThink2_1","PS1_0");
	oarc("TPS1_0","P_0");
	oarc("TThink2_1","PS1_2");
	oarc("TPS1_2","P_2");
	oarc("TThink2_1","PS1_3");
	oarc("TPS1_3","P_3");
	oarc("TThink2_2","PS2_0");
	oarc("TPS2_0","P_0");
	oarc("TThink2_2","PS2_1");
	oarc("TPS2_1","P_1");
	oarc("TThink2_2","PS2_3");
	oarc("TPS2_3","P_3");
	oarc("TThink2_3","PS3_0");
	oarc("TPS3_0","P_0");
	oarc("TThink2_3","PS3_1");
	oarc("TPS3_1","P_1");
	oarc("TThink2_3","PS3_2");
	oarc("TPS3_2","P_2");

	iarc("T0","P_0");
	oarc("T0","P_0p");
	iarc("T1","P_1");
	oarc("T1","P_1p");
	iarc("T2","P_2");
	oarc("T2","P_2p");
	iarc("T3","P_3");
	oarc("T3","P_3p");

	oarc("tcend","Cend");
  /* Inhibtor Arcs */ 
}
  /* GUARD Functions */  
int assert() {}
void ac_init() {
	pr_net_info();
}
void ac_reach() {
	pr_rg_info();
}
double holdingTime() {
 if(mark("Cend") == 0) 
 return(1.0); 
 else 
 return(0.0); } 
void ac_final() {
	pr_cum_expected("time in non-absorbing markings", holdingTime); 
}
