def create_SRN(f,N):
    file = open("exact_f%s_N%s_new_param_0421_script.c" % (f,N),'w+')
    file.write('#include <stdio.h>\n#include "user.h"\n')
    file.write('#include <stdlib.h>\n#include <time.h>\n')
    file.write("#define PR12Val1 7.7448 \n")
    file.write("#define PR12Val2 1.50895 \n")
    file.write("#define PR3Val1 34.7047 \n")
    file.write("#define PR3Val2 1.202207 \n")
    file.write("#define TxVal1 1.41605 \n")
    file.write("#define TxVal2 2.09217 \n")
    file.write("#define PR3HypoVal1 2 \n")
    file.write("#define PR3HypoVal2 267.9746 \n")
    file.write("#define PR3HypoVal3 22.04979 \n")
    file.write("#define PR3HypoVal4 0 \n")
    file.write("#define QVal1 25.864 \n")
    file.write("#define QVal2 1.56081 \n")
    file.write("#define QueueRate 8.959 \n")
    file.write("int f = %s; \n" % (f))
    file.write("int n = %s; \n" % (N))
    file.write("double timeVal; \n")

    # Functions
    file.write("int gPS0 () {\n")
    file.write("\tif ((mark(\"PP_0p\")>= 2*f)) return (1);\n");
    file.write("\t\telse\n");
    file.write("\treturn (0);}\n");

    for a in range(1,N):
        file.write("int gPS%s () {  \n" % (a))
        file.write("\tif ((mark(\"PP_%sp\")>= (2*f-1)))" % (a))
        file.write( "\treturn (1);\n    else\n    return (0);\n    }\n")

    for a in range(0,N):
        file.write("int gM%s () {  \n" % (a))
        file.write("\tif ((mark(\"P_%sp\")>= 2*f))" % (a))
        file.write("\treturn (1);\n    else\n    return (0);\n    }\n")

    file.write('int gCReply () { \n')
    file.write('\tif ((')

    for a in range(0,N):
        file.write("mark(\"PM%s\")" % (a))
        if a != N-1:
            file.write("+")
        a+=1
    file.write(">= (3*f+1)))\n")
    file.write("\treturn (1);\n    else\n    return (0);\n    }\n")

    file.write("int myhalt () {\n\tif (mark(\"Cend\") !=0)\n\t\treturn (0);\n\telse\n\t\treturn (1);\n}\n")

    #Options
    file.write('void options() { \n')
    file.write('\tiopt(IOP_SIMULATION,VAL_YES); \n')
    file.write('\tiopt(IOP_SIM_STD_REPORT,VAL_YES); \n')
    file.write('\tiopt(IOP_SIM_SEED,35983453) ; \n')
    file.write('\tiopt(IOP_SIM_RUNS, 5000); \n')
    file.write('\tfopt(FOP_SIM_ERROR, 0.1 ) ; \n')
    file.write('\tfopt(FOP_SIM_LENGTH, 20); \n')
    file.write('\tiopt(IOP_SIM_CUMULATIVE,VAL_NO); \n')
    file.write('\tfopt(FOP_SIM_CONFIDENCE,.9); \n')
    file.write('}\n')

    file.write('void net() {\n')
    for a in range(0,N):
        file.write("\tplace(\"PP_%s\");\n" % (a))
        file.write("\tplace(\"PP_%sp\");\n" % (a))
        file.write("\tplace(\"P_%s\");\n" % (a))
        file.write("\tplace(\"P_%sp\");\n" % (a))
        file.write("\tplace(\"M%s\");\n" % (a))
        file.write("\tinit(\"M%s\",1);\n" % (a))
        file.write("\tplace(\"PM%s\");\n" % (a))
        file.write("\tplace(\"PS%s\");\n" % (a))
        file.write("\tinit(\"PS%s\",1);\n" % (a))

    for a in range(1,N):
        for b in range(0,N):
            if b != a:
                file.write("\tplace(\"PPS%s_%s\");\n" % (a,b))
    file.write('\n')

    for a in range(0,N):
        for b in range(0,N):
            if b != a:
                file.write("\tplace(\"PS%s_%s\");\n" % (a,b))
    file.write('\n')

    file.write('\tplace("Cend");')
    file.write('\n')

    for a in range(1,N):
        file.write("\tplace(\"PPS%s\");\n" % (a))
        file.write("\tplace(\"N0%s\");\n" % (a))

    file.write('\tplace("Pcl");\n ')
    file.write('\tinit("Pcl",1);\n ')
    file.write('\tplace("CLeader");\n ')
    file.write('\tinit("CLeader",1);\n ')

    file.write('\timm("tcend"); guard("tcend",gCReply);')
    file.write('\tpriority("tcend",1); probval("tcend",1);\n')

    file.write('\tweibval("TC0",PR12Val1, PR12Val2);\n')

    for a in range(1,N):
        file.write("\tweibval(\"TN%s\",TxVal1, TxVal2);\n" % (a))
        file.write("\tweibval(\"TThink1_%s\",PR12Val1, PR12Val2);\n" % (a))
    file.write('\n')

    for a in range(1,N):
        for b in range(0,N):
            if b != a:
                file.write("\tweibval(\"TPPS%s_%s\",TxVal1, TxVal2);\n" % (a,b))
    file.write('\n')

    for a in range(0,N):
        file.write("\thypoval(\"TThink2_%s\",PR3HypoVal1, PR3HypoVal2,PR3HypoVal3, PR3HypoVal4);\n" % (a))
        file.write("\tguard(\"TThink2_%s\",gPS%s);\n" % (a,a))
    file.write('\n')

    for a in range(0,N):
        for b in range(0,N):
            if b != a:
                file.write("\tweibval(\"TPS%s_%s\",TxVal1,TxVal2);\n" % (a,b))
    file.write('\n')

    for a in range(0,N):
        file.write("\tweibval(\"TP%s\",QVal1,QVal2);\n" % (a))
        file.write("\tweibval(\"T%s\",QVal1,QVal2);\n" % (a))
        file.write("\timm(\"TM%s\");\n" % (a))
        file.write("\tguard(\"TM%s\",gM%s);\n" % (a,a))
        file.write("\tpriority(\"TM%s\",1);\n" % (a))
        file.write("\tprobval(\"TM%s\",1);\n" % (a))
    file.write('\thalting_condition(myhalt);\n')
    file.write('  /*  ======  ARC ====== */\n')

    for a in range(0,N):
        file.write("\tiarc(\"TM%s\",\"M%s\");\n" % (a,a))
        file.write("\toarc(\"TM%s\",\"PM%s\");\n" % (a,a))

    for a in range(1,N):
        file.write("\toarc(\"TN%s\",\"PPS%s\");\n" % (a,a))
        file.write("\toarc(\"TC0\",\"N0%s\");\n" % (a))
        file.write("\tiarc(\"TN%s\",\"N0%s\");\n" % (a,a))

    file.write('\tiarc("TC0","CLeader");\n')

    for a in range(1,N):
        for b in range(0,N):
            if b != a:
                file.write("\tiarc(\"TPPS%s_%s\",\"PPS%s_%s\");\n" % (a,b,a,b))
    file.write('\n')

    for a in range(0,N):
        for b in range(0,N):
            if b != a:
                file.write("\tiarc(\"TPS%s_%s\",\"PS%s_%s\");\n" % (a,b,a,b))
    file.write('\tiarc("tcend","Pcl");\n')

    for a in range(1,N):
        file.write("\tiarc(\"TThink1_%s\",\"PPS%s\");\n" % (a,a))
    file.write('\n')

    for a in range(0,N):
        file.write("\tiarc(\"TThink2_%s\",\"PS%s\");\n" % (a,a))
    file.write('\n')

    for a in range(1,N):
        for b in range(0,N):
            if b != a:
                file.write("\toarc(\"TPPS%s_%s\",\"PP_%s\");\n" % (a,b,b))
                file.write("\toarc(\"TThink1_%s\",\"PPS%s_%s\");\n" % (a,a,b))
    file.write('\n')

    for a in range(0,N):
        file.write("\tiarc(\"TP%s\",\"PP_%s\");\n" % (a,a))
        file.write("\toarc(\"TP%s\",\"PP_%sp\");\n" % (a,a))
    file.write('\n')

    for a in range(0,N):
        for b in range(0,N):
            if b != a:
                file.write("\toarc(\"TThink2_%s\",\"PS%s_%s\");\n" % (a,a,b))
                file.write("\toarc(\"TPS%s_%s\",\"P_%s\");\n" % (a,b,b))
    file.write('\n')

    for a in range(0,N):
        file.write("\tiarc(\"T%s\",\"P_%s\");\n" % (a,a))
        file.write("\toarc(\"T%s\",\"P_%sp\");\n" % (a,a))
    file.write('\n')

    file.write('\toarc("tcend","Cend");\n')
    file.write('  /* Inhibtor Arcs */ \n')
    file.write('}')
    file.write('\n')

    file.write('  /* GUARD Functions */  \n')

    file.write('int assert() {}\n')
    file.write('void ac_init() {\n\tpr_net_info();\n}\n')
    file.write('void ac_reach() {\n\tpr_rg_info();\n}\n')
    file.write('double holdingTime() {\n if(mark("Cend") == 0) \n return(1.0); \n else \n return(0.0); } \n')
    file.write('void ac_final() {\n')
    file.write('\tpr_cum_expected("time in non-absorbing markings", holdingTime); \n}\n')

create_SRN(1,4)
